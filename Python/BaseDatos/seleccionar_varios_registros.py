import psycopg2

conexion = psycopg2.connect(user='dakataca',
                password='51720',
                host='127.0.0.1',
                port='5432',
                database='test_db')

cursor = conexion.cursor()
sentencia = 'SELECT *  FROM persona WHERE id_persona IN %s'     # Devuelve varios registros.
#llaves_primarias = ((1,2,3),)      # Tupla de Tuplas.
entrada = input("Proporcione las identificaciones de la persona separadas por coma (,): ")
tupla = tuple(entrada.split(','))
print(tupla)
llaves_primarias = (tupla,)
#cursor.execute(sentencia, str(id_persona))  # Sentencia y parámetro a sustituir.
cursor.execute(sentencia, llaves_primarias)
registros = cursor.fetchall()      # Regresa varios registros.
#registros = cursor.fetchone()      # Regresa un registro.

#print(registros)

for registro in registros:
    print(registro)

cursor.close()
conexion.close()