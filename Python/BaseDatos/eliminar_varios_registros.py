import psycopg2

conexion = psycopg2.connect(user='dakataca',
                password='51720',
                host='127.0.0.1',
                port='5432',
                database='test_db')

cursor = conexion.cursor()
sentencia = 'DELETE FROM persona WHERE id_persona IN %s'
entrada = input("Proporcione los id_persona a eliminar separados por coma (,): ")
tupla = tuple(entrada.split(','))
valores = ( tupla, )
cursor.execute(sentencia, valores)
# Guardamos la información en la base de datos.
conexion.commit()
registros_eliminados = cursor.rowcount
print(f'Registros eliminados: {registros_eliminados}')
cursor.close()
conexion.close()