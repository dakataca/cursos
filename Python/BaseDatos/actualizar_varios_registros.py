import psycopg2

conexion = psycopg2.connect(user='dakataca',
                password='51720',
                host='127.0.0.1',
                port='5432',
                database='test_db')

cursor = conexion.cursor()
sentencia = 'UPDATE persona SET nombre = %s, apellido = %s, email = %s, telefono = %s  WHERE id_persona = %s'
valores = (
    ('Juan', 'Perez', 'jperez@mail.com', '3014568823', 1),
    ('Karla1', 'Gomez2', 'kgomez2@mail.com', '3014444444',2)
)
cursor.executemany(sentencia, valores)
# Guardamos la información en la base de datos.
conexion.commit()
registros_actualizados = cursor.rowcount
print(f'Registros actualizados: {registros_actualizados}')
cursor.close()
conexion.close()