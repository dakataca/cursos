import psycopg2

conexion = psycopg2.connect(user='dakataca',
                password='51720',
                host='127.0.0.1',
                port='5432',
                database='test_db')

cursor = conexion.cursor()
sentencia = 'INSERT INTO persona(nombre, apellido, email, telefono) VALUES(%s, %s, %s, %s)'
valores = (
    ('Marcos', 'Cantu', 'mcantu@mail.com', '3058694493'),
    ('Angel', 'Quintana', 'aquitana@mail.com', '3114303156'),
    ('María', 'Gonzales', 'mgonzales@mail.com', '3027789909')
)
cursor.executemany(sentencia, valores)
#cursor.execute(sentencia, valores)
# Guardamos la información en la base de datos.
conexion.commit()
registros_insertados = cursor.rowcount
print(f'Registros insertados: {registros_insertados}')
cursor.close()
conexion.close()