def suma(a, b):
    return a + b
print("La suma es:",suma(5, 3))


# Valores por defecto.
# Sino se proporcionan valores, se toman los asignados por defecto.
def suma(a=0, b=0):
    return a + b
print("La suma es:",suma())


print("La suma es:",suma(8, 13))