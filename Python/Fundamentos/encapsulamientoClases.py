class Persona:
    
    # __: Indica atributo privado.
    def __init__(self, n, e):
        self.__nombre = n
        self.__edad = e
    
    # getters
    def get_nombre(self):
        return self.__nombre
    
    def get_edad(self):
        return self.__edad
    
    #setters
    def set_nombre(self, n):
        self.__nombre = n
    
    def set_edad(self, e):
        self.__edad = e
        
p1 = Persona("Juan", 31)
#print(p1.__nombre)  Marca error porque atributo ahora es privado.
print(p1.get_nombre(), p1.get_edad())


#p1.nombre = "Karla"
#print(p1.__nombre) Marca error porque atributo ahora es privado.
p1.set_nombre("Karla")
p1.set_edad(30)
print(p1.get_nombre(), p1.get_edad())