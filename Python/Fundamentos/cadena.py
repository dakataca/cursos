cadena = "rebelde"
print("Mi grupo favorito es "+cadena)
print("Mi grupo favorito es",cadena)

# Concatenar de Strings.
numero1 = "1"
numero2 = "2"
print("Concatenación: " + numero1 + numero2)
print("Concatenación:", numero1 + numero2)

# Concatenar Strings y números.
num1 = 1
num2 = 2
print("Operación suma:", num1 + num2)
print("Operación suma:", (num1 + num2))
print("Operación suma:", + num1 + num2)
print("Operación suma: "+ str(num1 + num2))
print("Operación suma:", str(num1 + num2))
