class Persona:
# Crea la clase persona sin mayores cambios.
#    pass

    # Método automático init.
    def __init__(self, nombre="", edad=0):
        self.nombre = nombre
        self.edad = edad

print(Persona)

# Modificar valores.
Persona.nombre = "Daniel"
Persona.edad = 28

print(Persona.nombre)
print(Persona.edad)

# Crear nuevo objeto.
persona = Persona("Karla", 30)
print(persona.nombre)
print(persona.edad)
print(id(persona))


# Crear segundo objeto.
persona2 = Persona("Carlos", 40)
print(persona2.nombre)
print(persona.edad)
print(id(persona2))