# Imprimir emojis y símbolos.
error = {
    'xroja' : '\u274C',
    'xverde' : '\u274E'
    
}

warning = {
    'warning' : ''
}

validation = {
    'chulo_verde' : '\u2705',
    'chulo_light' : '\u2713',
    'chulo_bold' : '\u2714',
    'chulo_mini' : '\u2611'
}

number_circle_bold = {
    '1' : '\u2776',
    '2' : '\u2777',
    '3' : '\u2778',
    '4' : '\u2779',
    '5' : '\u277A',
    '6' : '\u277B',
    '7' : '\u277C',
    '8' : '\u277D',
    '9' : '\u277E',
    '10' : '\u277F'
}

number_circle = {
    '1' : '\u2780',
    '2' : '\u2781',
    '3' : '\u2782',
    '4' : '\u2783',
    '5' : '\u2784',
    '6' : '\u2785',
    '7' : '\u2786',
    '8' : '\u2787',
    '9' : '\u2788',
    '10' : '\u2789'
}

print('\ue215')

# Xbox.
print("\ue29d")

# Play station.
print("\ue230")

# python.
print("\ue235")

# Play Store.
print("\ue247")

# Java.
print("\ue256")

# Flecha =>.
print("\ue27a'")

print(error['xroja'])
print(error['xverde'])
print()
print(validation['chulo_verde'])
print(validation['chulo_light'])
print(validation['chulo_bold'])
print(validation['chulo_mini'])
print()
print(number_circle['1'])
print(number_circle['2'])
print(number_circle_bold['9'])
print(number_circle_bold['10'])
