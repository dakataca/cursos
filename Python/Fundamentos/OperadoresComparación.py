a = 4
b = 2

print("a = ",a)
print("b = ",b)
# Todos los operadores de comparación arrojan balor booleano.

# Igualdad.
resultado = a == b
print(a,"==",b,":",resultado)
print(a,"==",b,":"+str(resultado))

#Diferente.
resultado = a != b
print(a,"!=",b,":",resultado)
print(a,"!=",b,":"+str(resultado))

# Mayor.
resultado = a > b
print(a,">",b,":",resultado)
print(a,">",b,":"+str(resultado))

# Mayor o igual.
resultado = a >= b
print(a,">=",b,":",resultado)
print(a,">=",b,":"+str(resultado))

# Menor.
resultado = a < b
print(a,"<",b,":",resultado)
print(a,"<",b,":"+str(resultado))

# Menor o igual.
resultado = a <= b
print(a,"<=",b,":",resultado)
print(a,"<=",b,":"+str(resultado))


# Si el número es par.
if a%2 == 0:
    print(a,"es número par.")
else:
    print(a,"es número impar.")
    

# Si persona es mayor de edad.

edadLimite = 18
edadPersona = 17

if ( edadPersona >= edadLimite):
    print(edadPersona,"años es mayor de edad.")     
else:
    print(edadPersona,"años es menor de edad.")