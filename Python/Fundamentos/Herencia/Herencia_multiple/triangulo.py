from figura_geometrica import FiguraGeometrica
from color import Color

class Triangulo(FiguraGeometrica, Color):
    
    def __init__(self, base, altura, color):
        FiguraGeometrica.__init__(self, base, altura)
        Color.__init__(self, color)

    def __str__(self):
        return "Base: " + str(self.get_ancho()) + ", Altura: " + str(self.get_alto())

    def calcular_area(self):
        return (self.get_ancho() * self.get_alto())/2