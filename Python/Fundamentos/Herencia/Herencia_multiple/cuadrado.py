from figura_geometrica import FiguraGeometrica
from color import Color

class Cuadrado(FiguraGeometrica, Color):
    
    def __init__(self, lado, color):
        FiguraGeometrica.__init__(self, lado, lado)
        Color.__init__(self, color)
    
    def __str__(self):
        return "Lado: " + str(self.get_ancho()) + ", Color: " + self.get_color()
    
    def calcular_area(self):
        return self.get_ancho() * self.get_alto()
        
        
        