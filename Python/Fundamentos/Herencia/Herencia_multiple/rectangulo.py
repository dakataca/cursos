from figura_geometrica import FiguraGeometrica
from color import Color

class Rectangulo(FiguraGeometrica, Color):
    
    def __init__(self, ancho, alto, color):
        FiguraGeometrica.__init__(self, ancho, alto)
        Color.__init__(self, color)

    def __str__(self):
        return "Ancho: " + str(self.get_ancho() + ", Alto: " + self.get_alto() + ", color: " + self.get_color())

    def calcular_area(self):
        return self.get_ancho() * self.get_alto()

