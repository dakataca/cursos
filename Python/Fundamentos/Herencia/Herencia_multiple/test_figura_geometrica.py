from cuadrado import Cuadrado
from rectangulo import Rectangulo
from triangulo import Triangulo
#from figura_geometrica import FiguraGeometrica

# No es posible crear objetos de una clase abstracta.
#figuraGeometrica = FiguraGeometrica(2, 5)


cuadrado = Cuadrado(4, "rojo")
print(cuadrado.calcular_area())
print(cuadrado.get_color())

rectangulo = Rectangulo(3, 5, "Amarillo")
print(rectangulo.calcular_area())
print(rectangulo.get_color())

triangulo = Triangulo(2, 4, "Verde")
print(triangulo.calcular_area())
print(triangulo.get_color())



# Mostrar orden de ejecución de las clases.

print(Cuadrado.mro())