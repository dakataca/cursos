# Imptimir solo las letra a dentro de una cadena.

for letra in "Holanda":
    if letra == 'a':
        print(letra)
#        break
else:
    print("Fin ciclo for.")


# Imprimir solo números pares de un rango.
for i in range(6):
    if i%2 == 0:
        print(i)

# Imprimir solo números pares de un rango usando continue.
for i in range(6):
    if i%2 != 0:
        continue
    else:
        print(i)

# Imprimir solo números pares de un rango usando continue.
for i in range(6):
    if i%2 != 0:
        continue
    print(i)