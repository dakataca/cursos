class Persona:
    def __init__(self, nombre):
        self.__nombre = nombre

    # Modificar:
    # - Método de la clase padre Object.   
    # - Comportamiento de operador + para concatenar dos objetos.
    def __add__(obj1, obj2):
        return obj1.__nombre + ", " + obj2.__nombre

    # Modificar comportamiento de operador menos - .
    def __sub__(obj1, obj2):
        return "Operación no soportada."

    # Modificar comportamiento de operador multiplicar * .
    def __mul__(obj1, obj2):
        return "Operación no soportada."

    # Modificar comportamiento de operador dividir / .
    def __truediv__(obj1, obj2):
        return "Operación no soportada."

    # Modificar comportamiento de operador dividir enteros // .
    def __floordiv__(obj1, obj2):
        return "Operación no soportada."
    
    # Modificar comportamiento de operador módulo // .
    def __mod__(obj1, obj2):
        return "Operación no soportada."

    # Modificar comportamiento de operador de potencia ** .
    def __pow__(obj1, obj2):
        return "Operación no soportada."
 
p1 = Persona("Juan")
p2 = Persona("Karla")

# __add__  +: Adición.
print(p1 + p2)

# __sub__  -: Sustracción.
print(p1 - p2)

# __mul__  *: Multiplicación.
print(p1 * p2)

# __truediv__  /: División.
print(p1 / p2)

# __floordiv__  //: División entera.
print(p1 // p2)

# __mod__  %: Módulo.
print(p1 % p2)

# __pow__  **: Potencia.
print(p1 ** p2)