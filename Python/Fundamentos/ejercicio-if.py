mes = int(input("Digite el numero del mes (1-12): " ))
#estacion=None
estacion=""

if mes == 1 or mes == 2 or mes == 12:
    estacion = "Invierno"
elif mes >= 3 and mes <=5:
    estacion = "primavera"
elif mes >=6 and mes <=8 :
    estacion = "Verano"
elif mes >= 9 and mes <=11:
    estacion = "Otoño"
else:
    estacion = "Mes incorrecto"
    
print("Estación: ",estacion, "para el mes:",mes)