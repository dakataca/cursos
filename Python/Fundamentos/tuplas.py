# Una tubla mantiene el orden pero no se puede modificar,eliminar ni agregar elementos .

frutas = ("Naranja", "Plátano", "Guayaba")
print(frutas)

# Largo.
print(len(frutas), "elementos en la tupla.")

# Acceder al elemento de acuerdo al índice.
print(frutas[0],"=frutas[0]")
print(frutas[1],"=frutas[1]")
print(frutas[2],"=frutas[2]")


# Navegación inversa.
print(frutas[-1],"=Último elemento.")
print(frutas[-2],"=Penúltimo elemento.")

# Rangos
print("Imprimiendo rangos:")
print("0:2: Apartir del elemento en el índice 0, imprimir 2 elementos")
print(frutas[0:2])


## Modificar (no recomendado).
print("Cambiar elemento 'Plátano', por 'Platanito'")
# Convertir tupla en lista.
frutasLista = list(frutas)
# Modificar lista.
frutasLista[1]= "Platanito"
# Convertir lista en tupla.
frutas = tuple(frutasLista)
print("Nueva tupla:",frutas)


# Iterar tupla.
for fruta in frutas:
    #print(fruta)
    print(fruta,end=" ")
    
    
# Eliminar tupla.
del frutas
print("Imprimir tupla eliminada (debe dar error):",frutas)