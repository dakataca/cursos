nombres = ['Juan', 'Karla', 'Ricardo', 'María']
#nombres = ["Juan", "Karla", "Ricardo", "María"]
print(nombres)

# Largo de lista
print(len(nombres),": Largo de lista nombres.")

# Acceder a elemento de la lista.
print("\nAcceder a elementos de la lista:\n")
print(nombres[0],": pos[0]")
print(nombres[1],": pos[1]")
print(nombres[2],": pos[2]")
print(nombres[3],": pos[3]")


## Navegación inversa:

# Último elemento de la lista.
print(nombres[-1],": Último elemento de la lista")

# Penúltimo elemento de la lista.
print(nombres[-2],": Penúltimo elemento de la lista")


## Imprimir rangos:
print("\nImprimir rangos:\n")

# Imprimi a partir del elemento 0, dos elementos, es decir, los elementos 0 y 1.
print(nombres[0:2],"= 0:2")

# Imprimir 3 elementos desde el inicio, es decir, hasta el índice 2.
print(nombres[:3],"=  :3")


# Imprimir elementos hasta el final, desde el índice proporcionado.
print(nombres[1:],"= 1: ")

# Modificar el elemento del índice proporcionado.
print("Modificando elemento en la posición nombres[3]=",nombres[3])
nombres[3] = 'Ivoone'
print(nombres)

# Recorer lista con ciclo for
print("Recorriendo lista:")
for nombre in nombres:
    #print(nombre,end=" ")
    print(nombre)


# Comprobar si existe elemento en la lista.
print("Karla" in nombres)

if "Karla" in nombres:
    print("Karla sí existe en la lista.")
else:
    print("El elemento Karla no existe en la lista.")

# Agregar nuevo elemento.
print("Agregando elemento Lorenzo: ")
nombres.append("Lorenzo")
print(nombres)

# Agregar nuevo elemento en el índice proporcionado.
nombres.insert(1, "Octavio")
print(nombres)

# Remover un elemento.
print("Removiendo elemento Octavio...")
nombres.remove("Octavio")

# Remover último elemento.
print("Eliminando elemento ",nombres[-1],"...")
nombres.pop()
print(nombres)

# Remover elemento del índice proporcionado usando <del>.
del nombres[0]
print(nombres)

# Eliminar todos los elementos.
nombres.clear()
print(nombres)


# Eliminar por completo (incluído la variable) nuestra lista.
del nombres
print("Imprimiendo colección nombres (debe dar error):"+nombres)