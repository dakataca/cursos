class Validador:

    @staticmethod
    def validar_digit(entrada):
        """
        Valida si el número es un dígito y se encuentra dentro del rango permitido (1-4).

        Args:
        - entrada (str): El valor a validar.

        Return:
        - int: El número validado.

        Raises:
        - ValueError: Si la entrada es un caracter, no es un número válido o está fuera del rango permitido.
        """
        # Es alfabeto?
        if entrada.isalpha():
            raise ValueError("No se permiten caracteres del alfabeto.")
        else:
            try:
                # int -> float.
                # (!= int o float) => excepción.
                numero = float(entrada)
            except ValueError:
                # Valida caracteres especiales.
                raise ValueError("Se esperaba un número, no un caracter especial.")

            # Es int? [8.0, 1, 4, 6.0] => True.
            if numero.is_integer():
                # float -> int.
                digito = int(numero)
                # Dígito entre 1-4?
                if digito < 1 or digito > 4:
                    raise ValueError("Número fuera del rango (1-4)")
                else:
                    return digito
            else:
                raise ValueError("La entrada es un número decimal, se esperaba un dígito.")


    @staticmethod
    def cadena_vacia(cadena):
        """
        Valida si la cadena de caracteres está vacía.

        Args:
        - cadena (str): La cadena a validar.

        Return:
        - ValueError: Si la cadena está vacía o no es una cadena de caracteres.

        Raises:
        - ValueError: Si la cadena está vacía.
        """
        flag = True
        if not len(cadena) >= 1:
            raise ValueError("Nombre no puede estar vacío.")
        #elif not isinstance(cadena, str):
        #    raise ValueError("Se esperaba una cadena de caracteres (str).")
        else:
            flag = False
        return flag