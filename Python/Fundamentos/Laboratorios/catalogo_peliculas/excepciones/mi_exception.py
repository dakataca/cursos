#class MiException(Exception):
class MiException(ValueError):
    def __init__(self, msg):
        """
        Inicializa una instancia de la clase MiException.

        Args:
            msg (str): El mensaje de error asociado a la excepción.
        """
        self.message = msg
        #super().__init__(msg)
