from os import path, remove
import re

class CatalogoPeliculas:

    ruta_archivo = "Laboratorios/catalogo_peliculas/peliculas.txt"

    @classmethod
    def agregar_pelicula(cls, pelicula):
        """
        Agrega una película al catálogo de películas.

        Args:
            pelicula (Pelicula): La instancia de la clase Pelicula a agregar.

        Return:
            None.
        """            
        try:
            # Intentar abrir un fichero.
            buffer = open(cls.ruta_archivo, "a")
            buffer.write(pelicula.__str__()+ " ")
            print("Película "+pelicula.get_nombre()+" añadida!")
        except FileNotFoundError as e:
            print("Fichero de catálogo de películas no existe.")
        except Exception as e:
            print("Error al agregar contenido al fichero."+str(e))
        finally:
            buffer.close()

    @classmethod
    def listar_peliculas(cls):
        """
        Lista el contenido del fichero del catálogo de películas.

        Args:
            None.

        Returns:
            str: Contenido del fichero del catálogo de películas.
        """
        buffer = None  # Variable inicializada fuera del bloque try.
        try:
            # Intentar abrir un fichero.
            buffer = open(cls.ruta_archivo, "r")
            #print(type(buffer))
            print(buffer.read())
            #string = buffer.read()
        except FileNotFoundError as e:
            print("Error al listar catálogo de películas: Fichero no existe.")
        except Exception as e:
            print("Error al leer fichero."+str(e))
        finally:
            # Si el fichero no existe, no mande a cerrar el buffer.
            if buffer is not None:
                buffer.close()

    @classmethod
    def eliminar_fichero(cls):
        """
        Elimina el fichero que contiene el catálogo de películas.

        Args:
            None.

        Returns:
            None.
        """
        try:
            # Intentar remover un fichero.
            remove(cls.ruta_archivo)
            print("Fichero eliminado: "+cls.ruta_archivo)
        except FileNotFoundError as e:
            print("Error al intentar eliminar: Fichero de catálogo de películas no existe.")
        except Exception as e:
            print("Error al eliminar fichero.")

    @classmethod
    def pelicula_existe(cls, nombre_pelicula):
        """
        Verifica si una película existe en el catálogo de películas.

        Args:
            nombre_pelicula (str): El nombre de la película a verificar.

        Return:
            bool: True si la película existe, False en caso contrario.
        """
        buffer = None  # Variable inicializada fuera del bloque try.
        try:
            buffer= open(cls.ruta_archivo, "r")
            contenido = buffer.read()
            patron = r"\b" + re.escape(nombre_pelicula) + r"\b"
            return bool(re.search(patron, contenido))
        except FileNotFoundError as e:
            print("El archivo "+cls.ruta_archivo+" no existe.")
        except IOError as e:
            print("No se puede leer el archivo de catálogo de películas.")
        except Exception as e:
            print("Error validando archivo de catálogo de películas.")
        finally:
            # Si el fichero no existe, no mande a cerrar el buffer.
            if buffer is not None:
                buffer.close()
        return False
