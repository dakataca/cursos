class Pelicula:
    def __init__(self, nombre):
        """
        Inicializa una instancia de la clase Pelicula.

        Args:
            nombre (str): El nombre de la película.
        """
        self.__nombre = nombre

    def get_nombre(self):
        """
        Devuelve el nombre de la película.

        Returns:
            str: El nombre de la película.
        """
        return self.__nombre

    def set_nombre(self, nombre):
        """
        Establece un nuevo nombre para la película.

        Args:
            nombre (str): El nuevo nombre de la película.
        """
        self.__nombre = nombre

    def __str__(self):
        """
        Devuelve una representación en forma de cadena de la película.

        Returns:
            str: Una cadena que representa la película.
        """
        return "Película: " + self.get_nombre()