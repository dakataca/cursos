from dominio.peliculas import Pelicula
from servicio.catalogo_peliculas import CatalogoPeliculas
from excepciones.validador import Validador
from excepciones.mi_exception import MiException

def print_menu():
    """
    Imprime las posibles opciones que el usuario puede elegir.

    Returns:
        None
    """
    print("Opciones:")
    print("1) Agregar película.")
    print("2) Listar catálogo de películas.")
    print("3) Eliminar fichero de catálogo de películas.")
    print("4) Salir.")

def info_pelicula():
    """
    Valida la información de la película a agregar.
    Raises:
        MiException: Si la película ya existe.
        ValueError: Si el nombre de la película está vacío.
    """
    es_nombre_pelicula_vacio = True
    existe_pelicula = True
    # nombre vacío o película existe en catálogo?
    while es_nombre_pelicula_vacio == True or existe_pelicula == True:
        try:
            nombre_pelicula = input("Proporcione el nombre de la película a añadir al catálogo: ")
            # Nombre de película vacío?
            es_nombre_pelicula_vacio = Validador.cadena_vacia(nombre_pelicula)
            existe_pelicula = CatalogoPeliculas.pelicula_existe(nombre_pelicula)
            # nombre de película no está vacío y película no existe en catálogo?
            if existe_pelicula == False and es_nombre_pelicula_vacio == False:
                # Crear el objeto de la clase Película.
                movie = Pelicula(nombre_pelicula)
                # Agregar película al catálogo.
                CatalogoPeliculas.agregar_pelicula(movie)
            else:
                # CatalogoPeliculas.listar_peliculas()
                raise MiException("Película ya existe en catálogo.")
        except ValueError as e:
            print("Error de película: "+str(e))
        except Exception as e:
            print("Exception de nombre de película: "+str(e))


def main():
    # Creo variable que va a almacenar la opción del usaurio (1-4).
    option = 0
    # Mientras la opción que digita el usaurio sea distinto a salir (4).
    while option != 4:
        print_menu()
        try:
            option = input("Proporcione opción: ")
            # opción está entre rango 1-4? => Continua <> excepción.
            option = Validador.validar_digit(option)
            if option == 1:
                info_pelicula()
            elif option == 2:
                # Listar catálogo de peliculas.
                CatalogoPeliculas.listar_peliculas()
            elif option == 3:
                # Eliminar fichero del catálogo de peliculas.
                CatalogoPeliculas.eliminar_fichero()   
                # manejar_opciones_catalogo_peliculas(option)
        except ValueError as e:
            print("Error de validación:",str(e))
        except Exception as e:
            print("Error: "+str(e))
    else:
        print("Salimos del programa...")


main()