class Dispositivo:

    def __init__(self, tipo, tipoConector, marca):
        self._tipoDispositivo = tipo
        self._tipoConector = tipoConector
        self._marca = marca

    def get_tipoDispositivo(self):
        return self._tipoDispositivo

    def get_tipoConector(self):
        return self._tipoConector

    def get_marca(self):
        return self._marca

    def set_tipoDispositivo(self, tipo):
        self._tipoDispositivo = tipo

    def set_tipoConector(self, tipoConector):
        self._tipoConector = tipoConector

    def set_marca(self, marca):
        self._marca = marca

    def __str__(self):
        #return ", tipo: "+self.get_tipoDispositivo()+", conector: "+get_tipoConector()+", Marca: "+get_marca()       
        return(
            f"tipoDispositivo: {self.get_tipoDispositivo()}"
            f", tipoConector: {self.get_tipoConector()}"
            f", marca: {self.get_marca()}"
        )