from .dispositivo import Dispositivo
class Raton(Dispositivo):
    contadorRatones = 0
    def __init__(self, tipoDispositivo, tipoconector, marca):
        super().__init__(tipoDispositivo, tipoconector, marca)
        Raton.contadorRatones +=1
        self.__idRaton = Raton.contadorRatones

    def get_idRaton(self):
        return self.__idRaton

    def __str__(self):
        #return "\n\nidRaton: "+self.get_idRaton() \
        #    +super().__str__()
        return(
            f"Mouse=> Id: {self.get_idRaton()}"
            f", {super().__str__()}"
        )

#raton = Raton("entrada", "USB", "Delux")
#print(str(raton))
