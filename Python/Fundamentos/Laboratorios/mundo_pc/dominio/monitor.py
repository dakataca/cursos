from .dispositivo import Dispositivo
class Monitor(Dispositivo):
    contadorMonitores = 0
    def __init__(self, tipoDispositivo, tipoConector, marca, referencia, pulgadas, resolucion):
        super().__init__(tipoDispositivo, tipoConector, marca)
        Monitor.contadorMonitores +=1
        self.__idMonitor = Monitor.contadorMonitores
        self.__referencia = referencia
        self.__pulgadas = pulgadas
        self.__resolucion = resolucion

    def get_idMonitor(self):
        return self.__idMonitor

    def get_referencia(self):
        return self.__referencia

    def get_pulgadas(self):
        return self.__pulgadas

    def get_resolucion(self):
        return self.__resolucion

    def set_referencia(self, reference):
        self.__referencia = reference

    def set_pulgadas(self, inch):
        self.__pulgadas = inch

    def set_resolucion(self, resolution):
        self.__resolucion = resolution

    def __str__(self):
        #return "\t\tidMonitor: "+self.get_idMonitor() \
        #    +" "+self.get_referencia() \
        #    +" "+self.get_pulgadas() \
        #    +" "+self.get_resolucion() \
        #    +super().__str__()
        return(
            f"Monitor=> Id: {self.get_idMonitor()}"
            f", {self.get_referencia()}"
            f", {self.get_pulgadas()}\""
            f", {self.get_resolucion()}"
            f", {super().__str__()}"
        )

#monitor = Monitor("Salida", "VGA", "Samsung", "SyncMaster SA300", "18.5", "1366x768")
#print(str(monitor))