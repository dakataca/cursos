from .dispositivo import Dispositivo
class Teclado(Dispositivo):

    contadorTeclados = 0

    def __init__(self, tipoDispositivo, tipoConector, marca):
      super().__init__(tipoDispositivo, tipoConector, marca)
      Teclado.contadorTeclados +=1
      self.__idTeclado = Teclado.contadorTeclados

    def get_idTeclado(self):
        return self.__idTeclado

    def __str__(self):
        #return "\t\tidTeclado: "+str(self.get_idTeclado()) \
        #  +super().__str__()
        return(
          f"Teclado=> Id: {self.get_idTeclado()},"
          f" {super().__str__()}"
        )

#teclado = Teclado("entrada", "usb", "HP")
#print(str(teclado))