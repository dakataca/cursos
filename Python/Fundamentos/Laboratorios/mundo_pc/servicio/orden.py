class Orden:
    __contadorOrdenes = 0
    def __init__(self, computadoras):
        Orden.__contadorOrdenes +=1
        self.__idOrden = Orden.__contadorOrdenes
        self.__listaComputadoras = computadoras

    def get_idOrden(self):
        return self.__idOrden

    def get_listaComputadoras(self):
        return self.__listaComputadoras

    def __str__(self):
        str_computadoras = ""
        for computadora in self.get_listaComputadoras():
            str_computadoras += str(computadora)
        return(
            f"Orden: {self.get_idOrden()} \n"
            f" {str_computadoras}"
        )

    def agregarComputadora(self, pc):
        self.__listaComputadoras.append(pc)