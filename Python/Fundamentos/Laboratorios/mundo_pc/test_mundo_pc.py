from servicio.orden import Orden
from dominio.monitor import Monitor
from dominio.teclado import Teclado
from dominio.raton import Raton
from dominio.computadora import Computadora

mouse_delux = Raton("entrada","USB 2.0", "Delux")
teclado_redragon = Teclado("Entrada","USB 2.0","Redragon")
monitor_samsung = Monitor("salida","VGA","Samsung","SyncMaster SA300",18.5,"1366x768")
pc1 = Computadora(mouse_delux, teclado_redragon, monitor_samsung)
mouse_genius = Raton("entrada","USB 2.0", "Genius")
teclado_hp = Teclado("Entrada","USB 2.0","hp")
monitor_lg = Monitor("salida","HDMI","LG","U500",21,"1080x1080")
pc2 = Computadora(mouse_genius, teclado_hp, monitor_lg)

#listComputadoras = [pc1, pc2]
orden1 = Orden([pc1, pc2])
#orden1.get_listaComputadoras()
print(orden1)
pc3 = Computadora(mouse_delux,teclado_hp,monitor_samsung)
orden2 = Orden([pc1, pc2])
orden2.agregarComputadora(pc3)
#orden2.get_listaComputadoras()
print(orden2)