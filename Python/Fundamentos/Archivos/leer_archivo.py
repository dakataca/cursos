from os import remove

try:
    archivo = open("Archivos/prueba.txt", "r")
    # Ruta absoluta.
    #archivo = open("/home/dakataca/Git/GitLab/Cursos/Python/Fundamentos/Archivos/prueba.txt", "r")

    # Agregar información al fichero.
    #archivo.write("Primera línea.\n")
    #archivo.write("Segunda línea.\n")

    # Leer todo el contenido.
    #print(archivo.read())

    # Leer algunos caracteres.
    #print(archivo.read(5))    # Imprime "Agreg"
    #print(archivo.read(4))    # Imprime "amos"
    # Esto recuerda el último caracter leido, y de ahí continua.

    # Leer línea completa.
    #print(archivo.readline())
    # Siguiente línea.
    #print(archivo.readline())

    # Iterar líneas de archivo.
    #for line in archivo:
    #    print("Línea: "+line)

    # Iterar líneas de archivo.
    #for char in archivo.readlines():
    #    print(char)

    # Iterar caracteres de primera línea.
    #for char in archivo.readline():
    #    print(char)

    # Agrupar en una lista las líneas.
    list = archivo.readlines()
    print(list)

    # Imprimir línea específica.
    #print(archivo.readlines()[1])


    ## Parámetros:
    # r: Lectura.
    # w: Escritura (Borra el contenido y vuelve a escribir).
    # W+: Escritura y lectura.
    # a: Agrega (concatena), el nuevo contenido al fichero sin borrar.

    ## Copiar archivo en otro.
    #archivo2 = open("Archivos/copia.txt", "w+")
    #archivo2.write(archivo.read())

except Exception as e:
    print("Error al leer archivo.")
finally:
    archivo.close()
    #archivo2.close()

#print("Eliminando ficheros...")
#remove("Archivos/prueba.txt","Archivos/copia.txt")