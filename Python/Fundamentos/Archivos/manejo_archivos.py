try:
    # Si fichero no existe lo crea.
    # w: Escribir.
    archivo = open("Archivos/prueba.txt", "a")
    archivo.write("Agregamos info al archivo")
    archivo.write("\nAdios!")
except Exception as e:
    print("Error en archivo.")
finally:
    archivo.close()
    # Después de close() ya no podemos trabajar con el archivo.
    #archivo.write("saludos")
