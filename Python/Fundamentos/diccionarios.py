# Compuesto por elementos con llave y valor (key,value).
diccionario = {
    "IDE": "Integrated Development Environment",
    "OOP": "Object Oriented Programing",
    "DBMS": "Database Managment System"
}

print("\nImprimir diccionario:\n")
print(diccionario)

# Largo.
print("Largo de diccionario:",len(diccionario))

# Acceder a elemento según llave.
print("\nAcceder a elemento de acuerdo a la llave:\n")
print("Valor asociado a llave IDE:")
print(diccionario["IDE"])
print(diccionario.get("IDE"))


# Modificando valor de una llave.
print("\nModificar valor de llave IDE:\n")
diccionario["IDE"]= "Integrated development environment"
print(diccionario)

## Iterar diccionario.
print("\nIterar diccionario:\n")
for termino in diccionario:
    #print(termino)      # Imprime llaves.
    print(termino,end=" ")      # Imprime llaves.
#    print(diccionario[termino]) # Imprime valores.
    print(diccionario.get(termino)) # Imprime valores.

# Imprimir solo valores (sin llaves).
print("\nImprimir solo valores sin llaves.\n")
for valor in diccionario.values():
    print(valor)

# comprobar si elemento existe en el diccionario.
print("IDE" in diccionario)

print("Items:")
print("\nImprimir diccionario como items.\n")
for item in diccionario.items():
    print(item)

print()
# Agregar nuevos elementos.
diccionario["PK"] = "Primay Key"
print(diccionario)

# Remover elementos del diccionario.
diccionario.pop("DBMS")
print(diccionario)

# Limpiar diccionario.
diccionario.clear()
print(diccionario)

# Eliminar diccionario.
del diccionario
#print(diccionario)   # Debe dar error, porque se ha eliminado el diccionario.