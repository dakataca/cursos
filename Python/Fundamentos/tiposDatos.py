x = 5
y = 10.1
z = False
print(x)
print(y)
print(z)

# La funsión type no se puede mandar a llamar directamente en vscode.
type(x)

a = "Saludos!"
print(a)