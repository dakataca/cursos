from empleado import Empleado
from gerente import Gerente

def imprimir_detalles(obj):
    print(obj)
    print(type(obj), end="\n\n")
    # Si una variable es de la instancia que necesitamos.
    # Si tipo_padre es un objeto de la clase Gerente.
    if isinstance(obj, Gerente):
        print(obj.departamento)

empleado = Empleado("Juan", 1000.00)
imprimir_detalles(empleado)

empleado = Gerente("Karla", 2000.00, "Sistemas")
imprimir_detalles(empleado)