class Empleado:
    def __init__(self, nombre, sueldo):
        self.nombre = nombre
        self.sueldo = sueldo
#        self.__nombre = nombre
#        self.__sueldo = sueldo

    def get_nombre(self):
        return self.nombre
#        return self.__nombre

    def set_nombre(self, nombre):
        self.nombre = nombre
#        self.__nombre = nombre

    def get_sueldo(self):
        return self.sueldo
#        return self.__sueldo

    def set_sueldo(self, sueldo):
        self.sueldo = sueldo
#        self.__sueldo = sueldo

    def __str__(self):
        return "Nombre: " + self.get_nombre() + ", sueldo: $"+ str(self.get_sueldo())