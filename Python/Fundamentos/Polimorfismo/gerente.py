from empleado import Empleado

class Gerente(Empleado):
    def __init__(self, nombre="", sueldo=0.0, dpto=""):
        super().__init__(nombre, sueldo)
        self.departamento = dpto
#        self.__departamento = dpto

    def get_departamento(self):
        return self.departamento
#        return self.__departamento

    def set_departamento(self, dpto):
        self.departamento = dpto
#        self.__departamento = dpto

    def __str__(self):
        return super().__str__()+ ", Departamento: "+ self.get_departamento()