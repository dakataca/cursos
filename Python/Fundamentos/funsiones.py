
## Parámetros opcionales en una funsión:
# - Son valores por defecto que adoptan los parámetros de una funsión, en caso que no se le asignen.

# var=0: Entero.
# var="": String.
# var=0.0: Float.
# var=[]: Lista.
# *var: Tupla.
# var=(): Tupla.
# **var: Diccionario.
# var={}: Diccionario.

# Cualquier parámetro sin valor por defecto es obligatorio, y si no se declara, manda excepción TypeError.
def saludar(saludo, nombre, edad=0):
#def saludar(saludo, nombre, edad):
    print("Digo", saludo, "a", nombre+" "+str(edad))
    print(type(edad))

saludar("hola", "Karla")
saludar("hola", "Karla",34)
saludar(nombre="Daniel", saludo="Hola")
saludar(nombre="Daniel", saludo="Hola", edad=35)