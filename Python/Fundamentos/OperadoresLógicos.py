# Rangos.
#a = 2

a = int(input("Proporciona un número:"))
valorMinimo = 0
valorMaximo = 6
dentroRango = (a >= valorMinimo and a <= valorMaximo)
#print(dentroRango)

if(dentroRango):
    print(a,"está entre",valorMinimo,"y",valorMaximo)
else:
    print(a,"no está entre",valorMinimo,"y",valorMaximo)

# Vacaciones.

vacaciones = False
diaDescanso = True

if(vacaciones or diaDescanso):
    print("Podemos ir al parque.")
else:
    print("Tengo deberes que hacer.")

canchaDisponible = True
jugadoresCompletos = True
PlayStationDisponible = False
 
if((canchaDisponible and jugadoresCompletos) or PlayStationDisponible):
    print("Podemos ir a jugar.")
else:
    print("No podemos ir a jugar.")
    
# Negación.
print("PlayStationDisponible negado!",not(PlayStationDisponible))  
if(not(canchaDisponible and jugadoresCompletos) or PlayStationDisponible):
    print("Podemos ir a jugar.")
else:
    print("No, no podemos ir a jugar.")
