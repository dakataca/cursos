class MiClase:
    
    variable_clase = "Variable clase"
    
    def __init__(self):
        self.variable_instancia = "variable_instancia"


    # No necesita objeto.
    # No puede acceder a variables o métodos de instancia (self) porque aun no se han creado objetos.
    @staticmethod
    def metodo_estatico():
        print("Método estático")
        print(MiClase.variable_clase)
        # Desde un método estático no podemos acceder a una variable de instancia.
        #print(MiClase.variable_instancia)

    # No necesita objeto.
    # No puede acceder a variables o métodos de instancia (self) porque aun no se han creado objetos.
    # Acceso a solo los atributos y métodos de clase.
    @classmethod
    def metodo_clase(cls):
        print("Método de clase:" + str(cls))
        print(cls.variable_clase)
        # Desde un contexto estático (Clase) no podemos acceder a una variable de instancia.
        #print(cls.variable_instancia)

    # Podemos acceder a:
    # - Métodos estáticos.
    # - Métodos de clase.
    def metodo_instancia(self):
        self.metodo_estatico()
        self.metodo_clase()
        print(self.variable_instancia)
        print(self.variable_clase)

MiClase.metodo_estatico()
MiClase.metodo_clase()

print()
# Desde el contexto de instancia (dinámico) podemos accerder a:
# - Métodos estáticos.
# - Métodos de clase.
miclase = MiClase()
miclase.metodo_estatico()
miclase.metodo_clase()