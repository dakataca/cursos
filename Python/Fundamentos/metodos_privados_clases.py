class Persona:
    
    def __init__(self, nombre, ape_paterno, ape_materno):
        self.nombre = nombre
        self._ape_paterno = ape_paterno
        self.__ape_materno = ape_materno
        
        
    def get_ape_paterno(self):
        return self._ape_paterno
    
    def get_ape_materno(self):
        return self.__ape_materno
    
    def set_ape_paterno(self, ape_paterno):
        self._ape_paterno = ape_paterno
        
    def set_ape_materno(self, ape_materno):
        self.__ape_materno = ape_materno
        
    def metodo_publico(self):
        self.__metodo_privado()
    # _: Indica que variable es protegida o medianamente privada.
    # __: Indica que variable es privada.
    # Método privado.
    def __metodo_privado(self):
        print(self.nombre)
        print(self._ape_paterno)
        print(self.__ape_materno)
        
p1 = Persona("Juan", "Lopez", "Perez")
#p1.__metodo_privado() Marca error porque es un métido privado.
p1.metodo_publico()   # Imprime método privado a través de un método público.

# Accediendo a variables directamente.
print(p1.nombre)  # Atributo público.
print(p1._ape_paterno) # Atributo protegido.

# Usando getters and setters.
print(p1.nombre)
print(p1.get_ape_paterno())
print(p1.get_ape_materno())