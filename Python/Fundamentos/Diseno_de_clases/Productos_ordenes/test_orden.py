from orden import Orden
from producto import Producto

producto1 = Producto("Camisa", 100.00)
producto2 = Producto("Pantalón", 200.00)
producto3 = Producto("Calcetines", 300.00)


productos = [producto1]
#print(productos)

orden1 = Orden(productos)
print(orden1)
print("Total a pagar orden1: $"+str(orden1.calcular_total_pagar()))

#productos.append(producto3)
orden2 = Orden(productos)
orden2.add_producto(producto2)
print(orden2)
print("Total a pagar orden2: $"+str(orden2.calcular_total_pagar()))

orden3 = Orden(productos)
orden3.add_producto(producto3)
print(orden3)
print("Total a pagar orden3: $"+str(orden3.calcular_total_pagar()))
