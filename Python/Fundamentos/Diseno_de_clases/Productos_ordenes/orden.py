class Orden:

    contador = 0

    def __init__(self, productos):
        # Cada que se crea un objeto Orden, aumenta contador de órdenes en 1.
        Orden.contador += 1
        # Id de cada producto va a ser igual al valor del contador de órdenes (consecutivo).
        self.__id_orden = Orden.contador
        self.__productos = productos

    def get_id(self):
        return self.__id_orden

    def set_id(self, id):
        self.__id_orden = id

    def get_productos(self):
        return self.__productos

    def set_productos(self, productos):
        self.__productos = productos

    def productos_to_str(self):
        productos_str = ""
        for producto in self.get_productos():
            # Añadir cada objeto de la lista a un str.
            productos_str += " "+str(producto)
        return productos_str

    def add_producto(self, producto):
        self.__productos.append(producto)
        #productos = self.get_productos()
        #productos.append(producto)
        #self.set_productos(productos)

    def calcular_total_pagar(self):
        total_pagar=0.0
        for producto in self.get_productos():
            total_pagar += producto.get_precio()
        return total_pagar

    def __str__(self):
        return "Orden: "+ str(self.get_id()) + ", Productos: "+ str(self.productos_to_str()) + " | "

        # Añadir cada objeto de la lista a un str.
        #productos_str = ""
        #for producto in self.get_productos():
        #    productos_str += producto.__str__() + " | "
        #return "Orden: " + str(self.get_id()) + ", Productos: " + productos_str
