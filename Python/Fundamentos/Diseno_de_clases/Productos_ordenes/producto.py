class Producto:

    contador = 0

    def __init__(self, nombre, precio):
        # Cada que se crea un objeto Producto, aumenta contador de productos en 1.
        Producto.contador += 1
        # Id de cada producto va a ser igual al valor del contador de productos (consecutivo).
        self.__id_producto = Producto.contador
        self.__nombre = nombre
        self.__precio = precio

    def get_id(self):
        return self.__id_producto

    def set_id(self, id):
        self.__id_producto = id

    def get_nombre(self):
        return self.__nombre

    def set_nombre(self, nombre):
        self.__nombre = nombre

    def get_precio(self):
        return self.__precio

    def set_precio(self, precio):
        self.__precio = precio

    def __str__(self):
        return "idProducto: "+str(self.get_id())+ " , nombre: "+ self.get_nombre()+ " , precio: "+ str(self.get_precio())