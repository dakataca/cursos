from numeros_identicos_exception import NumerosIdenticosException

# Variable a usar fuera del bloque try.
resultado = None

try:
    #a = 10
    #a = "10"
    #b = 0
    a = float(input("Digite un número: "))
    b = float(input("Digite otr número: "))
    resultado = a / b

    if a== b:
        raise NumerosIdenticosException("Números idénticos.")
# Clases de menor jerarquía primero.
except ZeroDivisionError as e:
#except Exception as e:
    print("Error, división entre cero '0' ZeroDivisionError.", e)
    print(type(e))
except TypeError as e:
    print("Error, tipo de dato TypeError.", e)
except ValueError as e:
    print("Error, tipo de dato ValueError.", e)
    print(type(e))
except Exception as e:
    print("Ocurrió un error Exception.", e)
    print(type(e))
else:
    print("No hubo ninguna excepción.")
finally:
    # Siempre se va a ejecutar, aunque no se atrape una excepción.
    print("Fin del manejo de excepciones.")

print("Resulado: "+str(resultado))
print("continuamos...")