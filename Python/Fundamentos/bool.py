# Tipos de datos booleanos (true o false).

x = True
print(x)

y = False
print(y)

num1 = 3
num2 = 2
resulado = num1 < num2
print(resulado)

if(num1 < num2):
    print("Número 1 es menor que número 2.")
else:
    print("Número 1 no es menor que número 2")
    