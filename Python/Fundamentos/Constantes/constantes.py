# Van en ficheros separados.
# Siempre en mayúsculas.
MI_CONSTANTE = "Valor de mi constante"


class Matematicas:
    
    # Dentro de una clase una constante debe estar fuera de cualquier método.
    PI = 3.1416

#print(MI_CONSTANTE)
