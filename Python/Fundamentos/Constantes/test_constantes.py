from constantes import MI_CONSTANTE, Matematicas as Mate
#from constantes import Matematicas

print(MI_CONSTANTE)
print(Mate.PI)

# Comprobando que en Python las constantes se puede modificar, por lo que no son realmente constantes.
MI_CONSTANTE = "Nuevo valor"
Mate.PI = 3.14
print(MI_CONSTANTE,"\n"+str(Mate.PI))