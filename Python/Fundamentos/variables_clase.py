class MiClase:
    variable_clase = "Variable de clase"

    def __init__(self, variable_instancia=''):
        self.variable_instancia = variable_instancia

# Con el objeto puedo acceder tanto variables de instancia como a variables de clase.
miclase = MiClase('Variable instancia')

# Podemos acceder a las variables de clase desde:
# - la clase propia.
# - objetos.
print("Acceso a variables de clase:")
print("- Clase:", MiClase.variable_clase)
print("- Objeto:", miclase.variable_clase)

# Podemos acceder a las variables de instancia solo desde objetos.
print("\nAcceso a variables de instancia:")
print("- Objeto:", miclase.variable_instancia)


# No se puede acceder a variable de instancia desde la clase.
#print(MiClase.variable_instancia)  # Debe dar error.

# Convertir variable de instancia a de clase.
#para tener acceso desde la clase MiClase.
MiClase.variable_instancia = 'Modificando variable de instancia'
print("\nClase: "+ MiClase.variable_instancia)   # Variable de clase.
print("Objeto: "+ miclase.variable_instancia)   # Variable de objeto.

# Modificar variable de clase desde el objeto.
miclase.variable_clase = "Modificando variable de clase desde Objeto miclase."
# Mismas variables pero valores distintos en objeto miclase y en clase MiClase.
print("\n"+ miclase.variable_clase)
print(MiClase.variable_clase)

miclase2 = MiClase("Nuevo valor de instancia (Objeto)")
# Como valor de variable de clase no ha sido modificado desde objeto miclase2,
#entonces, toma el valor que tiene en la clase.
print(miclase2.variable_clase)


miclase3 = MiClase("Tecer Objeto")

MiClase.variable_clase = "Cambio de variable clase desde clase MiClase."
# Toma valor modificado en el objeto miclase.
print(miclase.variable_clase)
# Toma valor de la clase MiClase, porque no ha sido modificada variable_clase, desde el objeto miclase2.
print(miclase2.variable_clase)
# Toma valor de la clase MiClase, porque no ha sido modificada variable_clase, desde el objeto miclase3.
print(miclase3.variable_clase)