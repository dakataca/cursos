# Colecciones tipo set.
# - No mantienen ningun orden.
# - No manejan índices.
# - Son elementos únicos, no permiten datos duplicados.
# - No permiten ser modificados.

planetas = {"Marte","Jupiter","Venus"}
print(planetas)

# Largo.
print(len(planetas),"elementos en la colección desordenada de planetas.")

# Consultar si elemento está en la colección desordenada.
print( "Marte" in planetas)

# Agregar elemento.
planetas.add("Tierra")
print(planetas)

# Validar duplicados.
planetas.add("Tierra")
print(planetas)


## Eliminar elementos.

# Eliminar con remove genera excepción si no encuentra elemento.
planetas.remove("Saturno")
print(planetas)

# Eliminar con discard no genera excepción si no encuentra elemento.
planetas.discard("Júpiters")
print(planetas)

# Vaciar o limpiar set.
planetas.clear()
print(planetas)

# Eliminar set.
del planetas
print(planetas)